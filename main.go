package main

import (
	"codeberg.org/shizzuru/shenanigans/linkedlist"
)

const (
	iter = 0
)

func main() {
	vals := [iter]int{}
	for i := 0; i < iter; i++ {
		vals[i] = i
	}

	lany := linkedlist.NewAnySinglyLinkedList(func(a, b int) bool { return a == b })
	l := linkedlist.NewSinglyLinkedList[int]()
	for _, v := range vals {
		lany.PushBack(v)
		l.PushBack(v)
	}

	l.FindAll(50)
	lany.FindAll(50)
}
