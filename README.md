# shenanigans library

Currently only implements linked list shenanigans featuring
 - No reflection
 - Completely generic
 - Compatible with "any" type
 - Unbreakable, type safe list
