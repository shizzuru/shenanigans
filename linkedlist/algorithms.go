package linkedlist

func findAllAny[T any](v T, e Element[T], equal equalFunc[T]) []Element[T] {
	rs := []Element[T]{}
	for !e.isValueNil() {
		if equal(e.Value(), v) {
			rs = append(rs, e)
		}
		e = e.nextElem()
	}
	return rs
}

func findAll[T comparable](v T, e Element[T]) []Element[T] {
	rs := []Element[T]{}
	for !e.isValueNil() {
		if e.Value() == v {
			rs = append(rs, e)
		}
		e = e.nextElem()
	}
	return rs
}

func back[T any](e Element[T]) Element[T] {
	if e.isValueNil() {
		return nil
	}
	for !e.nextElem().isValueNil() {
		e = e.nextElem()
	}
	return e
}
