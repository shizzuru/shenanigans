package linkedlist

type equalFunc[T any] func(a, b T) bool

type Element[T any] interface {
	Value() T
	nextElem() Element[T]
	isValueNil() bool
}

type List[T any] interface {
	PushFront(T)
	PushBack(T)
	Find(T) Element[T]
	FindAll(T) []Element[T]
	Front() Element[T]
	Back() Element[T]
}
