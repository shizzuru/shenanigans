package linkedlist

type singlyLinkedElementAny[T any] struct {
	value T
	next  *singlyLinkedElementAny[T]
}

func (e *singlyLinkedElementAny[T]) Value() T {
	return e.value
}

func (e *singlyLinkedElementAny[T]) nextElem() Element[T] {
	return e.next
}

func (e *singlyLinkedElementAny[T]) isValueNil() bool {
	return e == nil
}

type singlyLinkedListAny[T any] struct {
	ll    *singlyLinkedElementAny[T]
	equal equalFunc[T]
}

func (sll *singlyLinkedListAny[T]) PushFront(v T) {
	sll.ll = &singlyLinkedElementAny[T]{
		value: v,
		next:  sll.ll,
	}
}

func (sll *singlyLinkedListAny[T]) PushBack(v T) {
	if sll.ll == nil {
		sll.ll = &singlyLinkedElementAny[T]{
			value: v,
		}
		return
	}
	curr := sll.ll
	for curr.next != nil {
		curr = curr.next
	}
	curr.next = &singlyLinkedElementAny[T]{
		value: v,
	}
}

func (sll *singlyLinkedListAny[T]) Find(v T) Element[T] {
	curr := sll.ll
	for curr != nil && !sll.equal(curr.value, v) {
		curr = curr.next
	}
	return nil
}

func (sll *singlyLinkedListAny[T]) FindAll(v T) []Element[T] {
	return findAllAny[T](v, sll.ll, sll.equal)
}

func (sll *singlyLinkedListAny[T]) Front() Element[T] {
	return sll.ll
}

func (sll *singlyLinkedListAny[T]) Back() Element[T] {
	if sll.ll == nil {
		return nil
	}
	return back[T](sll.ll)
}

func NewAnySinglyLinkedList[T any](equal equalFunc[T]) List[T] {
	return &singlyLinkedListAny[T]{
		equal: equal,
	}
}
