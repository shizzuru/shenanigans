package linkedlist

type singlyLinkedElement[T comparable] struct {
	value T
	next  *singlyLinkedElement[T]
}

func (e *singlyLinkedElement[T]) Value() T {
	return e.value
}

func (e *singlyLinkedElement[T]) nextElem() Element[T] {
	return e.next
}

func (e *singlyLinkedElement[T]) isValueNil() bool {
	return e == nil
}

type singlyLinkedList[T comparable] struct {
	ll *singlyLinkedElement[T]
}

func (sll *singlyLinkedList[T]) PushFront(v T) {
	sll.ll = &singlyLinkedElement[T]{
		value: v,
		next:  sll.ll,
	}
}

func (sll *singlyLinkedList[T]) PushBack(v T) {
	toInsert := &singlyLinkedElement[T]{
		value: v,
	}
	if sll.ll == nil {
		sll.ll = toInsert
		return
	}
	curr := sll.ll
	for curr.next != nil {
		curr = curr.next
	}
	curr.next = toInsert
}

func (sll *singlyLinkedList[T]) Find(v T) Element[T] {
	curr := sll.ll
	for curr != nil && curr.value != v {
		curr = curr.next
	}
	return curr
}

func (sll *singlyLinkedList[T]) FindAll(v T) []Element[T] {
	return findAll[T](v, sll.ll)
}

func (sll *singlyLinkedList[T]) Front() Element[T] {
	return sll.ll
}

func (sll *singlyLinkedList[T]) Back() Element[T] {
	if sll.ll == nil {
		return nil
	}
	return back[T](sll.ll)
}

func NewSinglyLinkedList[T comparable]() List[T] {
	return &singlyLinkedList[T]{}
}
